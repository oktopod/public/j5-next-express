Ceci est un projet démo [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Vous pouvez démarrer le projet avec les instructions suivantes:

```bash
npm run dev
# Run le Projet NEXT sans IOT
npm run iot
# Run le Projet uniquement en IOT
npm run simulate
# Run le projet IOT et le projet NEXT en même tps
```

Ce projet met en place un SERVER EXPRESS pour permettre la communication entre la partie "Admin" Next et la partie "Arduino J5"

Ouvrir [http://localhost:3000](http://localhost:3000) avec votre navigateur pour afficher la partie Admin Next (React)

Le code permettant de DEV sur Arduino est situé dans le dossier IOT.

Vous pouvez modifier la page de démonstration suivante `app/page.js`. Le watcher auto update en mode DEV.

La démo contient également l'exemple permettant de démarrer un son MP3 à partir d'une interface.

Il est possible de repartir de ce dépôt pour facilement déployer sur les RASPBERRY, un clone sur les RP4 suffira pour ensuite lancer les projets.

Attention, si vous souhaitez utiliser cette base, bien laisser NextJS en version 13, la version 14 n'est pas encore pris en charge sur les RP4 utilisé en démonstration.

## Pour en savoir plus

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
