import express from 'express';
import temporal from 'temporal';
import five from 'johnny-five';
import { createServer } from "http";
import { Server } from "socket.io";

console.log('-- API IOT INIT --');
const port = 3001;
const board = new five.Board();

// Server SOCKET et HTTP
const app = express();

const server = createServer(app);
const io = new Server(server, {
    cors: {
        origin: "*"
    }
});

io.on('connection', (socket) => {
    console.log('a user connected');
});

function Index() {

    board.on("ready", () => {
        console.log('-- BOARD READY --');
        console.log('-- Init Composant --');

        const led = new five.Led.RGB([9, 10, 11]);
        led.color("#FFFFFF");

        const proximity = new five.Proximity({
            controller: "HCSR04",
            pin: 7
        });

        console.log('-- Check EVENT --');
        proximity.on("change", () => {
            const { centimeters } = proximity;
            //console.log("  cm  : ", centimeters);
            io.emit('distance', centimeters);
        });


        // app.get('/led/red', function (req, res) {
        //     console.log("100% red");
        //     led.color("#FF0000");

        app.get('/led/color/:color', function (req, res) {
            const color = req.params.color;
            console.log("100% " + color);
            led.color("#" + color);
            res.send("100% " + color);
        });

    });
    server.listen(port, () => {
        console.log(`IOT Listen on Port ${port}`)
    })
}
Index();
//     // Initialize the RGB LED
//     const led = new five.Led.RGB([6, 5, 3]);

//     // Set to full intensity red
//     console.log("100% red");
//     led.color("#FF0000");

//     temporal.queue([{
//         // After 3 seconds, dim to 30% intensity
//         wait: 3000,
//         task() {
//             console.log("30% red");
//             led.intensity(30);
//         }
//     }, {
//         // 3 secs then turn blue, still 30% intensity
//         wait: 3000,
//         task() {
//             console.log("30% blue");
//             led.color("#0000FF");
//         }
//     }, {
//         // Another 3 seconds, go full intensity blue
//         wait: 3000,
//         task() {
//             console.log("100% blue");
//             led.intensity(100);
//         }
//     }]);
// });


// board.on("ready", () => {
//     // respond with "hello world" when a GET request is made to the homepage
//     app.get('/', function (req, res) {

//         const led = new five.Led(13);
//         //led.blink(500);

//         let rep = {
//             "code": 200,
//             "message": "Votre première API"
//         }
//         res.send(rep);
//     });

//     app.get('/led/on', function (req, res) {
//         var led = new five.Led(13);
//         led.on();
//         res.send('LED ON');
//     });

//     app.get('/led/off', function (req, res) {
//         var led = new five.Led(13);
//         led.off();
//         res.send('LED OFF');
//     });

//     //Sensor.eventlisten()

//     app.listen(port, () => {
//         console.log(`Example app listening on port ${port}`)
//     })

// });