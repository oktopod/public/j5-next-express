"use client";
import { useState } from "react";

export default function Index() {
  const [data, setData] = useState("STATE ");
  return (
    <div>
      <button
        onClick={() => {
          callLed("on");
        }}
      >
        Allumer
      </button>
      <button
        onClick={() => {
          callLed("off");
        }}
      >
        Eteindre
      </button>
      <section>Contenu de la page dashboard {data}</section>
    </div>
  );
}
