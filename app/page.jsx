"use client";
import { useEffect, useState } from "react";
import io from "socket.io-client";

console.log("init SOCKET");
export default function Home() {
  const [color, setColor] = useState("#111111");
  const [audio, setAudio] = useState(null);
  const [intensity, setIntensity] = useState(0);

  const handlePlay = (event) => {
    audio.play();
  };

  const handleChange = (event) => {
    // 👇 Get input value from "event"
    console.log(event.target.value);
    setColor(event.target.value);
  };

  //Initialisation de la PAGE
  useEffect(() => {
    // Set SOCKET Communication
    const socket = io(getHost() + ":3001/");
    socket.on("distance", (...args) => {
      let distFF = convertHex(args);
      setColor("#" + distFF + distFF + distFF);
      //console.log(convertHex(args));
    });
    // Set AUDIO
    setAudio(new Audio("/son.mp3")); // only call client
  }, []);

  useEffect(() => {
    console.log(color);
  }, [color]);
  return (
    <main
      className="flex min-h-screen flex-col items-center justify-evenly p-24"
      style={{ backgroundColor: color }}
    >
      <div>
        <input
          type="color"
          pattern="#[a-f0-9]{6}"
          value={color}
          onChange={handleChange}
        />
        <button onClick={handlePlay}>Lecture</button>
      </div>
    </main>
  );
}

function convertHex(value) {
  if (value > 100) value = 100;
  let result = Math.floor((value / 100) * 255).toString(16);
  if (result.length == 1) result = "0" + result;
  return result;
}

function getHost() {
  if (typeof window !== "undefined") {
    const protocol = window.location.protocol;
    const host = window.location.hostname;
    const currentHost = protocol + "//" + host;
    return currentHost;
  } else return "/";
}
